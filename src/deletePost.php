<?php

session_start();
// Verifie si un utilisateur est connecté
if(empty($_SESSION['mail'])){
    header("Location: ../views/connection.php");
    return;
}

// Verifie si l'utilisateur est admin
if($_SESSION['isAdmin'] === "1"){
    require('../conf/db_conf.php');
    $db = $base->prepare('DELETE FROM post WHERE post_id = :id');
    $db->bindParam(':id', $_GET['id']);
    $db->execute();
    $_SESSION['success'] = "Post supprimé avec succès";
    header("Location: ../views/admin.php");
} else {
    // Retourne vers la page d'amin si pas admin
    $_SESSION['error'] = "Non autorisé";
    header("Location: ../views/admin.php");
    return;
}
