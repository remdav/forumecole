<?php

// Retourne tous les utilisateurs
class getAllUser {
    function getAll(){
        session_start();
        if($_SESSION['isAdmin'] === "1"){
            require('../conf/db_conf.php');
            $db = $base->prepare('SELECT * FROM user');
            $db->execute();
            return $db;
        } else {
            echo "Non autorisé";
            return;
        }
    }
}