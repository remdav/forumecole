<?php

// Classe qui permet d'ajouter un post
class Add {
    protected $title;
    protected $message;
    protected $isError;

    public function __construct()
    {
        $this->setMessage();
        $this->setTitle();
    }

    public function setTitle(){
        if(strlen($_POST['title']) < 5 || strlen($_POST['title'] >= 50)){
            $this->setIsError(true);
            return;
        }

        $this->title = $_POST['title'];
    }

    public function getTitle(){
        return $this->title;
    }

    public function setMessage(){
        if(strlen($_POST['message']) < 5){
            $this->setIsError(true);
            return;
        }

        $this->message = $_POST['message'];
    }

    public function getMessage(){
        return $this->message;
    }

    public function setIsError($state){
        $this->isError = $state;
    }

    public function getIsError(){
        return $this->isError;
    }

    public function addPost(){
        session_start();
        // verifie si les champs ont été renseignés
        if(empty($_SESSION['id']) && empty($_SESSION['mail'])){
            header('Location: ../views/connection.php');
            return;
        }

        // Si un des champs ne respect pas les conditions pour les entrer dans la base de données puis retourne les informations à l'utilisateur
        if($this->getIsError()){
            session_start();
            $_SESSION['error'] = 'Merci de respect les nombre miniumum (5) de caratctères et maximum !';
            header('Location: ../views/add.php');
            return;
        }

        // Verifie si un des champs n'as pas été renseigné
        if(empty($this->getMessage()) || empty($this->getTitle())){
            $_SESSION['error'] = 'Merci de saisir des informations dans tous les champs !';
            header('Location: ../views/add.php');
            return;
        }

        require('../conf/db_conf.php');
        
        //Préparation de la requête préparé puis envoie les infos dans la base
        $create = $base->prepare('INSERT INTO post (title, message, user_id) VALUE (:title, :message, :user_id )');
        $create->bindParam(':title', $this->getTitle());
        $create->bindParam(':message', $this->getMessage());
        $create->bindParam(':user_id', $_SESSION['id']);
        $create->execute();
        header('Location: ../views/admin.php');
    }

    public function addPostBase(){
        // test que l'insertion n'as pas eu de problème 
        try{
            $this->addPost();
        } catch (Exception $e) {
            echo "erreur";
        }
    }
}

$add = new Add();
$add->addPostBase();

