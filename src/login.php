<?php

// Gestion des connexions
class Login {
    protected $email;
    protected $password;
    protected $isError;

    public function __construct()
    {
        $this->setEmail();
        $this->setPassword();
    }

    public function setEmail(){
        // Verifie si l'adresse mail est correct
        if(preg_match('/^[A-Z0-9][A-Z0-9._%+-]{0,63}@(?:[A-Z0-9](?:[A-Z0-9-]{0,62}[A-Z0-9])?\.){1,8}[A-Z]{2,63}$/', $_POST['email'])){
            $this->setIsError(true);
            return;
        }

        $this->email = $_POST['email'];
    }

    public function getEmail(){
        return $this->email;
    }

    public function setPassword(){
        // Verifie si le password respect les regles
        if(strlen($_POST['password']) < 5 || strlen($_POST['password'] >= 50)){
            $this->setIsError(true);
            return;
        }

        $this->password = $_POST['password'];
    }

    public function getPassword(){
        return $this->password;
    }

    public function hashPassword(){
        return hash('sha256',  "nevot".$this->getPassword());
    }

    public function setIsError($state){
        $this->isError = $state;
    }

    public function getIsError(){
        return $this->isError;
    }

    public function connectUser(){
        // Verifie si les password correspondenr
        if(empty($this->getEmail()) || empty($this->hashPassword())){
            echo $this->getEmail();
            return;
            session_start();
            $_SESSION['error'] = "Merci de saisir un login/MDP";
            header('Location:../views/connection.php');
            return;
        }

        // Si un des champs ne respect pas les conditions pour les entrer dans la base de données puis retourne les informations à l'utilisateur
        if($this->getIsError()){
            session_start();
            $_SESSION['error'] = 'Merci de respecter tous les champs !';
            header('Location: ../views/register.php');
            return;
        }

        require('../conf/db_conf.php');
        include('../conf/conf.php');
        $db = $base->prepare("SELECT email, password, user_id, admin, nickname FROM user WHERE email = :email and password = :password");
        $db->execute(array(
            'email' => $this->getEmail(),
            'password' => $this->hashPassword(),
        ));

        // Recupere dans des variables les informations contenu dans la base
        foreach($db as $row){
            $email = $row['email'];
            $id = $row['user_id'];
            $admin = $row['admin'];
            $nickname = $row['nickname'];
        }

        // si le mot de passe n'est pas le bon
        if(!$email){
            session_start();
            $_SESSION['error'] = "Mauvais mot de passe !";
            header('Location:../views/connection.php');
            return;
        }

        session_start();
        $_SESSION['id'] = $id;
        $_SESSION['isAdmin'] = $admin;
        $_SESSION['mail'] = $this->getEmail();
        $_SESSION['isConnect'] = true;
        $_SESSION['nickname'] = $nickname;
        header('Location:../views/admin.php' );
    }
}

$login = new Login();
$login->connectUser();