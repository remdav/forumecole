<?php

class post{
    // Retourne les posts appropriés
    public function getPost(){
        session_start();
        // Verifie si un utilisateur est connecté ou si c'est l'index qui veut l'information
        if(empty($_SESSION['mail'] || isset($_SESSION['index']))){
            header("Location: connection.php");
        }

        require(dirname(__FILE__).'/../conf/db_conf.php');
        // Verifie si un utilisateur est connecté
        if($_SESSION['isAdmin'] !== "1" && $_SESSION['mail'] ){
            $db = $base->prepare('SELECT * FROM post INNER JOIN user ON post.user_id = user.user_id WHERE post.user_id ='.$_SESSION['id']);
            $db->execute();
            unset($_SESSION['index']);
            // Verifie si l'utilisateur est admin ou l'index
        } else if($_SESSION['isAdmin'] === "1" || $_SESSION['index'] === true) {
            $db = $base->prepare('SELECT * FROM post INNER JOIN user ON post.user_id = user.user_id');
            $db->execute();
            unset($_SESSION['index']);
        }

        return $db;
    }
}

