<?php

// Permet de s'inscrire sur le site
class Subscribe {
    protected $name;
    protected $firstname;
    protected $nickname;
    protected $email;
    protected $password;
    protected $passwordConfirm;
    protected $isError;

    public function __construct()
    {
        $this->setName();
        $this->setFirstname();
        $this->setNickname();
        $this->setEmail();
        $this->setPassword();
        $this->setPasswordConfirm();
    }

    public function setName(){
        // Verifie si les conditions sont d'insertions sont valides
        if(strlen($_POST['name']) < 2 || strlen($_POST['name']) >= 50){
            $this->setIsError(true);
            return;
        }

        $this->name = $_POST['name'];
    }

    public function getName(){
        return $this->name;
    }

    public function setFirstname(){
        // Verifie si les conditions sont d'insertions sont valides
        if(strlen($_POST['firstname']) < 2 || strlen($_POST['firstname']) >= 50){
            $this->setIsError(true);
            return;
        }

        $this->firstname = $_POST['firstname'];
    }

    public function getFirstname(){
        return $this->firstname;
    }

    public function setNickname(){
        // Verifie si les conditions sont d'insertions sont valides
        if(strlen($_POST['nickname']) < 2 || strlen($_POST['nickname']) >= 50){
            $this->setIsError(true);
            return;
        }

        $this->nickname = $_POST['nickname'];
    }

    public function getNickname(){
        return $this->nickname;
    }

    public function setEmail(){
        // Verifie si les conditions sont d'insertions sont valides
        if(preg_match('/^[A-Z0-9][A-Z0-9._%+-]{0,63}@(?:[A-Z0-9](?:[A-Z0-9-]{0,62}[A-Z0-9])?\.){1,8}[A-Z]{2,63}$/', $_POST['email'])){
            $this->setIsError(true);
            return;
        }

        $this->email = $_POST['email'];
    }

    public function getEmail(){
        return $this->email;
    }

    public function setPassword(){
        // Verifie si les conditions sont d'insertions sont valides
        if(strlen($_POST['password']) < 5 || strlen($_POST['password']) >= 50){
            $this->setIsError(true);
            return;
        }

        $this->password = $_POST['password'];
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPasswordConfirm(){
        // Verifie si les conditions sont d'insertions sont valides
        $this->passwordConfirm = $_POST['passwordConfirm'];
    }

    public function getPasswordConfirm(){
        return $this->passwordConfirm;
    }

    public function hashPassword(){
        // Hash le mot de passe
        return hash('sha256',  "nevot".$this->getPassword());
    }

    public function setIsError($state){
        $this->isError = $state;
    }

    public function getIsError(){
        return $this->isError;
    }

    public function createUser(){
        // Verifie si tous les champs ont une valeur
        if(empty(empty($this->getName()) || empty($this->getFirstname()) || empty($this->getNickname()) || empty($this->getEmail()) || $this->getPasswordConfirm()) || empty($this->getPassword())){
            session_start();
            $_SESSION['error'] = 'Merci de saisir des informations dans tous les champs !';
            header('Location: ../views/register.php');
            return;
        }

        // Verifie que toutes les condition sont correcte
        if($this->getIsError()){
            session_start();
            $_SESSION['error'] = 'Merci de respecter tous les champs !';
            header('Location: ../views/register.php');
            return;
        }

        // Verifie les deux mot de passes
        if($this->getPassword() !== $this->getPasswordConfirm()){
            session_start();
            $_SESSION['errorPassword'] = 'Les mots de passe ne correspondent pas !';
            header('Location: ../views/register.php');
            return;
        }

        include('../conf/conf.php');
        require('../conf/db_conf.php');
        $create = $base->prepare('INSERT INTO user (name, firstname, nickname, email, password) VALUE (:name, :firstname, :nickname, :email, :password)');
        $create->bindParam(':name', $this->getName());
        $create->bindParam(':firstname', $this->getFirstname());
        $create->bindParam(':nickname', $this->getNickname());
        $create->bindParam(':email', $this->getEmail());
        $create->bindParam(':password', $this->hashPassword());
        $create->execute();
        // Generation des headers avant envoie
        $header = 'From: contact@forum-damblemont-viot.yj.fr' . "\r\n";
        $header .= 'Reply-To: contact@forum-damblemont-viot.yj.fr' . "\r\n";
        $header .= 'Content-type: text/html; charset= utf8\n';
        // Envoie le mail de confirmation
        mail($this->getEmail(), utf8_encode("Compte cree avec sujet"), "Félicitation, votre compte à été créé avec succès. Voici votre identifiant est votre adresse mail :". $this->getEmail(), $header);
        header('Location: ../views/connection.php');
    }

    public function addUser(){
        // Verifie que tout se passe bien
        try{
            $this->createUser();
        } catch (Exception $e) {
            return;
        }
    }

}

$createUser = new Subscribe();
$createUser->addUser();