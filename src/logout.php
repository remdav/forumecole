<?php

class Logout{
    // Detruit toutes les session
    public function logoutUser(){
        include('../conf/conf.php');
        session_start();
        unset($_SESSION['isConnect']);
        session_destroy();
        header('Location:'.$urlIndex);
    }
}

$logout = new Logout();
$logout->logoutUser();