<?php
    require('./src/listpost.php');
    session_start();
    $_SESSION['index'] = true;
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <link rel="icon" href="assets/images/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="Projet PHP au sein de l'institut g4" />
        <meta name="keywords" content="PHP, ecole, school, mickael-martin-nevot, project, projet" />
        <title>Forum G4 Rémy & Anthony</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="assets/css/materialize.css" type="text/css" rel="stylesheet" />
        <link href="assets/css/style.css" type="text/css" rel="stylesheet" />
        <!--  Scripts-->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/materialize.js"></script>
        <script src="assets/js/init.js"></script>
        <script src="assets/js/top.js"></script>

    </head>
    <body>
    <a href="#topPage" id="return-to-top"><i class="material-icons">change_history</i></a>
    <nav class="light-blue lighten-1" id="topPage">
        <div class="nav-wrapper container"><a id="logo-container" href="#" class="brand-logo">Forum-G4</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="views/connection.php">Se connecter</a></li>
                <li><a href="views/register.php">S'inscrire</a></li>
            </ul>

            <ul id="nav-mobile" class="sidenav">
                <li><a href="views/connection.php"><i class="material-icons">account_circle</i>Se connecter</a></li>
                <li><a href="views/register.php"><i class="material-icons">subscriptions</i>S'inscrire</a></li>
            </ul>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>
    <div class="section no-pad-bot" id="index-banner">
        <div class="container">
            <br><br>
            <h1 class="header center blue-text">Projet forum G4</h1>
            <div class="row center">
                <h5 class="header col s12 light">Ceci est le projet Forum proposé par Mr NEVOT</h5>
            </div>

        </div>
    </div>


    <div class="container">
        <div class="section">

            <!--   Icon Section   -->
            <div class="row">
                <div class="col s12 m6">
                    <div class="icon-block">
                        <h2 class="center light-blue-text"><i class="material-icons">flash_on</i></h2>
                        <h5 class="center">Developpement en 6 jours</h5>
                    </div>
                </div>

                <div class="col s12 m6">
                    <div class="icon-block">
                        <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
                        <h5 class="center">Interface agréable</h5>
                    </div>
                </div>
            </div>

        </div>
        <br><br>
    </div>

    <div class="container">
        <div class="section">
            <div class="row">
                <h1 class="header center blue-text">Les posts de sur notre forum</h1>
                <?php
                    $post = new Post();
                    foreach ($post->getPost() as $row){
                ?>
                <div class="col s12 m6 offset-s3">
                        <div class="row">
                        <div class="col m10">
                            <div class="card blue lighten-1">
                                <div class="card-content black-text">
                                    <span class="card-title"><? echo $row['title']; ?></span>
                                    <p><?php echo $row['message']; ?></p>
                                </div>
                                <hr/>
                                <div class="card-action white-text right-align">
                                    Créé par: <? echo $row['name'].' '.$row['firstname']; ?>
                                </div>
                                <div class="card-action white-text right-align">
                                    le <?
                                        echo date("m/d/y à g:i", strtotime($row['post_date']));
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </div>

    <footer class="page-footer blue lighten-1">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Rémy & anthony</h5>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                Fait par <a class="orange-text text-lighten-3" href="http://remydamblemont.fr">Remy</a>&Anthony
            </div>
        </div>
    </footer>

    </body>
</html>
