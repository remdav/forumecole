<?php
require('../src/getAllUsers.php');
session_start();
if($_SESSION['isAdmin'] === "1"){
    if($_SESSION['success']){
        ?><div style="background-color: green"> <? echo $_SESSION['success'] ?> </div><?php
        unset($_SESSION['success']);
    }

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" href="../assets/images/favicon.ico" />
        <meta name="description" content="Projet PHP au sein de l'institut g4" />
        <meta name="keywords" content="PHP, ecole, school, mickael-martin-nevot, project, projet, admin, administrator" />
        <title>Gestion des utilisateurs</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="../assets/css/materialize.css" type="text/css" rel="stylesheet" />
        <link href="../assets/css/style.css" type="text/css" rel="stylesheet" "/>
        <!--  Scripts-->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/materialize.js"></script>
        <script src="../assets/js/init.js"></script>
        <script src="../assets/js/top.js"></script>
    </head>
    <body>
        <a href="#topPage" id="return-to-top"><i class="material-icons">change_history</i></a>
        <nav class="light-blue lighten-1" id="topPage">
            <div class="nav-wrapper container"><a id="logo-container" href="../forum.php" class="brand-logo center">Forum-G4</a>
                <ul class="left hide-on-med-and-down">
                    <li>Connecté : <?php echo $_SESSION['nickname'] ?></li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li><a href="../views/admin.php">Gestion des posts</a></li>
                    <li><a href="../src/logout.php">Se déconnecter</a></li>
                </ul>
                <ul id="nav-mobile" class="sidenav">
                    <li class="center-align light-blue lighten-1">Connecté : <?php echo $_SESSION['nickname'] ?></li>
                    <li><a href="../views/admin.php">Gestion des posts</a></li>
                    <li><a href="../src/logout.php">Se déconnecter</a></li>
                </ul>
                <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <div class="container">
            <div class="section">
                <table>
                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Pseudo</th>
                        <th>email</th>
                        <th>Administrateur</th>
                        <th>Ajouter</th>
                        <th>Supprimer</th>
                    </tr>
                    <?php
                    $user = new getAllUser();
                    foreach ($user->getAll() as $row){
                        ?>
                        <tr>
                            <td><? echo $row['name']; ?></td><?php
                            ?><td><? echo $row['firstname']; ?></td><?php
                            ?><td><? echo $row['nickname']; ?></td><?php
                            ?><td><? echo $row['email']; ?></td><?php
                            ?><td><? echo $row['admin']; ?></td><?php
                            ?><td><a class="btn-large waves-light green darken-2" href="../views/register.php">Ajouter</a></td>
                            <td><a class="btn-large waves-light red darken-2" href="../src/deleteUser.php?id=<?php echo $row['user_id'] ?>">Supprimer</a></td>
                        </tr><?php
                    }
                    ?>
                </table>
            </div>
    </body>
</html>
<?php
} else {
    header('location: ../views/connection.php');
}
