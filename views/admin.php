<?php
    session_start();
    if($_SESSION['isConnect'] === true){
        if($_SESSION['success']){
            ?><div style="background-color: green"> <? echo $_SESSION['success'] ?> </div><?php
            unset($_SESSION['success']);
        }

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" href="../assets/images/favicon.ico" />
        <meta name="description" content="Projet PHP au sein de l'institut g4" />
        <meta name="keywords" content="PHP, ecole, school, mickael-martin-nevot, project, projet, admin, administrator" />
        <title>Admin</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="../assets/css/materialize.css" type="text/css" rel="stylesheet" />
        <link href="../assets/css/style.css" type="text/css" rel="stylesheet" />
        <!--  Scripts-->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/materialize.js"></script>
        <script src="../assets/js/init.js"></script>
        <script src="../assets/js/top.js"></script>
    </head>
    <body>
        <a href="#topPage" id="return-to-top"><i class="material-icons">change_history</i></a>
        <nav class="light-blue lighten-1" id="topPage">
            <div class="nav-wrapper container"><a id="logo-container" href="../forum.php" class="brand-logo center">Forum-G4</a>
                <ul class="left hide-on-med-and-down">
                    <li>Connecté : <?php echo $_SESSION['nickname'] ?></li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <?php if($_SESSION['isAdmin'] === "1"){ ?>
                        <li><a href="../views/users.php">Gestion utilisateurs</a></li>
                    <?php } ?>
                    <li><a href="../src/logout.php">Se déconnecter</a></li>
                </ul>
                <ul id="nav-mobile" class="sidenav">
                    <li class="center-align light-blue lighten-1">Connecté : <?php echo $_SESSION['nickname'] ?></li>
                    <?php if($_SESSION['isAdmin'] === "1"){ ?>
                        <li><a href="../views/users.php">Gestion utilisateurs</a></li>
                    <?php } ?>
                    <li><a href="../src/logout.php">Se déconnecter</a></li>
                </ul>
                <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <div class="container">
            <div class="section">
                <table class="highlight col s12">
                    <tr>
                        <th>Titre</th>
                        <th>Message</th>
                        <th>Créateur</th>
                        <th>Date</th>
                        <th>Supprimer</th>
                    </tr>
                    <?php
                        require('../src/listpost.php');
                        $post = new Post();
                        foreach ($post->getPost() as $row){
                        ?><tr><td class="col s2"><? echo $row['title']; ?></td><?php
                        ?><td class="col s2"><textarea class="materialize-textarea" disabled><? echo $row['message']; ?></textarea></td><?php
                        ?><td class="col s2"><? echo $row['name'].' '.$row['firstname']; ?></td</tr><?php
                        ?><td class="col s2"><? echo $row['post_date']; ?></td</tr><?php
                        ?><td><a class="col s1 btn-large waves-light red darken-2" href="../src/deletePost.php?id=<? echo $row['post_id'] ?>">Supprimer</a></td</tr><?php
                    }
                    ?>
                </table>
            </div>
            <div class="row center">
                <a class="btn-large waves-light teal lighten-1" href="add.php">Ajouter un post</a>
            </div>
        </div>
        <div class="container">
            <div class="section">
                <div class="row">
                    <h1 class="header center blue-text">Les posts disponible sur notre forum</h1>
                    <?php
                    require('../src/getAllPost.php');
                    $post = new getAllPost();
                    foreach ($post->allPost() as $row){
                        ?>
                        <div class="col s12 m6 offset-s3">
                            <div class="row">
                                <div class="col m10">
                                    <div class="card blue lighten-1">
                                        <div class="card-content black-text">
                                            <span class="card-title"><? echo $row['title']; ?></span>
                                            <p><?php echo $row['message']; ?></p>
                                        </div>
                                        <hr/>
                                        <div class="card-action white-text right-align">
                                            Créé par: <? echo $row['name'].' '.$row['firstname']; ?>
                                        </div>
                                        <div class="card-action white-text right-align">
                                            le <?
                                            echo date("m/d/y à g:i", strtotime($row['post_date']));
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
} else {
    header('Location: connection.php');
}
?>