<?php
    session_start();
    if(isset($_SESSION['error'])){
        ?><div style="background-color: red"> <? echo $_SESSION['error'] ?> </div><?php
        session_destroy();
    }

    if($_SESSION['success']){
        ?><div style="background-color: green"> <? echo $_SESSION['success'] ?> </div><?php
        unset($_SESSION['success']);
    }

    if($_SESSION['isConnect'] === true){

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" href="../assets/images/favicon.ico" />
        <meta name="description" content="Projet PHP au sein de l'institut g4" />
        <meta name="keywords" content="PHP, ecole, school, mickael-martin-nevot, project, projet, add, post" />
        <title>Ajouter un post</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="../assets/css/materialize.css" type="text/css" rel="stylesheet" />
        <link href="../assets/css/style.css" type="text/css" rel="stylesheet" />
        <!--  Scripts-->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/materialize.js"></script>
        <script src="../assets/js/init.js"></script>
    </head>
    <body>
        <nav class="light-blue lighten-1">
            <div class="nav-wrapper container"><a id="logo-container" href="../forum.php" class="brand-logo center">Forum-G4</a>
                <ul class="left hide-on-med-and-down">
                    <li>Connecté : <?php echo $_SESSION['nickname'] ?></li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li><a href="../views/admin.php">Les post</a></li>
                </ul>
                <ul id="nav-mobile" class="sidenav">
                    <li class="center-align light-blue lighten-1">Connecté : <?php echo $_SESSION['nickname'] ?></li>
                    <li><a href="../views/admin.php">Les post</a></li>
                </ul>
                <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <form action="../src/add.php" method="post">
            <div class="container">
                <div class="row">
                    <div class="input-field col s10">
                        <input id="title" name="title" type="text" class="validate" minlength="5" maxlength="50">
                        <label for="title">Titre</label>
                        <span class="helper-text" data-error="Minimum 5 caractères et maximum 50 caractères"></span>
                    </div>
                    <div class="input-field col s10">
                        <textarea id="message" name="message" type="text" minlength="5" class="materialize-textarea validate"></textarea>
                        <label for="message">Message</label>
                        <span class="helper-text" data-error="Minimum 5 caractères"></span>
                    </div>
                </div>
            </div>
            <div class="row center">
                <input type="submit" class="btn-large waves-light green lighten-1" content="Se connecter">
            </div>
        </form>
    </body>
</html>
<?php
    } else {
        header('Location: connection.php');
    }
?>