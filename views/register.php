<?php
    session_start();
    if(isset($_SESSION['error'])){
        ?><div style="background-color: red"> <? echo $_SESSION['error'] ?> </div><?php
        session_destroy();
    }

    if(isset($_SESSION['errorPassword'])){
        ?><div style="background-color: red"> <? echo $_SESSION['errorPassword'] ?> </div><?php
        session_destroy();
    }
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" href="../assets/images/favicon.ico" />
        <meta name="description" content="Projet PHP au sein de l'institut g4" />
        <meta name="keywords" content="PHP, ecole, school, mickael-martin-nevot, project, projet" />
        <title>S'inscrire</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="../assets/css/materialize.css" type="text/css" rel="stylesheet" />
        <link href="../assets/css/style.css" type="text/css" rel="stylesheet" />
        <!--  Scripts-->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/materialize.js"></script>
        <script src="../assets/js/init.js"></script>
    </head>
    <body>
        <nav class="light-blue lighten-1" >
            <div class="nav-wrapper container"><a id="logo-container" href="../forum.php" class="brand-logo">Forum-G4</a>
                <ul class="right hide-on-med-and-down">
                    <?php if($_SESSION['isAdmin'] === "1"){
                        ?>
                            <li><a href="../views/users.php">Gestion utilisateurs</a></li>
                            <li><a href="../src/logout.php">Se déconnecter</a></li>
                        <?php
                    } else {
                        ?> <li><a href="../forum.php"><i class="material-icons">home</i></a></li> <?php
                        ?> <li><a href="../views/connection.php">Se connecter</a></li> <?php
                    }
                    ?>
                </ul>
                <ul id="nav-mobile" class="sidenav">
                    <?php if($_SESSION['isAdmin'] === "1"){
                        ?>
                        <li><a href="../views/users.php">Gestion utilisateurs</a></li>
                        <li><a href="../src/logout.php">Se déconnecter</a></li>
                        <?php
                    } else {
                        ?> <li><a href="../forum.php"><i class="material-icons">home</i>Accueil</a></li> <?php
                        ?> <li><a href="../views/connection.php"><i class="material-icons">account_circle</i>Se connecter</a></li> <?php
                    }
                    ?>
                </ul>
                <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <form action="../src/subscribe.php" method="post" class="col s12">
            <div class="container">
                <div class="row">
                    <div class="input-field col s6">
                        <input id="name" name="name" type="text" class="validate" minlength="2" maxlength="50" pattern="^[a-zA-Z0-9._\s]{1,50}$" required>
                        <label for="name">Nom</label>
                        <span class="helper-text" data-error="Minimum 2 caractères et maximum 50 caractères"></span>
                    </div>
                    <div class="input-field col s6">
                        <input id="firstname" name="firstname" type="text" class="validate" minlength="2" maxlength="50" pattern="^[a-zA-Z0-9._\s]{1,50}$" required>
                        <label for="firstname">Prénom</label>
                        <span class="helper-text" data-error="Minimum 2 caractères et maximum 50 caractères"></span>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="input-field col s6">
                        <input id="nickname" name="nickname" type="text" class="validate" minlength="2" maxlength="50" pattern="^[a-zA-Z0-9._\s]{1,50}$" required>
                        <label for="nickname">Pseudo</label>
                        <span class="helper-text" data-error="Minimum 2 caractères et maximum 50 caractères"></span>
                    </div>
                    <div class="input-field col s6">
                        <input id="email" name="email" type="email" class="validate" minlength="5" maxlength="150" pattern="/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/" required>
                        <label for="email">Adresse email</label>
                        <span class="helper-text" data-error="Merci de saisir une adresse mail valide"></span>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="input-field col s6">
                        <input id="password" name="password" type="password" class="validate" minlength="5" maxlength="50" required>
                        <label for="password">Mot de passe</label>
                        <span class="helper-text" data-error="Minimum 5 caractères et maximum 50 caractères"></span>
                    </div>
                    <div class="input-field col s6">
                        <input id="passwordConfirm" name="passwordConfirm" type="password" class="validate" minlength="5" maxlength="50" required>
                        <label for="passwordConfirm">Confirmation du mot de passe</label>
                        <span class="helper-text" data-error="Minimum 5 caractères et maximum 50 caractères"></span>
                    </div>
                </div>
            </div>
            <div class="center">
                <input type="submit" class="btn-large waves-light teal lighten-1" content="Se connecter">
            </div>
        </form>
    </body>
</html>