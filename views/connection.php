<?php
    session_start();
    if(isset($_SESSION['error'])){
        ?><div style="background-color: red"> <? echo $_SESSION['error'] ?> </div><?php
       session_destroy();
    }
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" href="../assets/images/favicon.ico" />
        <meta name="description" content="Projet PHP au sein de l'institut g4" />
        <meta name="keywords" content="PHP, ecole, school, mickael-martin-nevot, project, projet" />
        <title>Forum G4 Rémy & Anthony</title>

        <!-- CSS  -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="../assets/css/materialize.css" type="text/css" rel="stylesheet" />
        <link href="../assets/css/style.css" type="text/css" rel="stylesheet" />
        <!--  Scripts-->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/materialize.js"></script>
        <script src="../assets/js/init.js"></script>
    </head>
    <body>
        <nav class="light-blue lighten-1">
            <div class="nav-wrapper container"><a id="logo-container" href="../forum.php" class="brand-logo">Forum-G4</a>
                <ul class="right hide-on-med-and-down">
                    <li><a href="../forum.php"><i class="material-icons">home</i></a></li>
                    <li><a href="../views/register.php">S'inscrire</a></li>
                </ul>
                <ul id="nav-mobile" class="sidenav">
                    <li><a href="../forum.php"><i class="material-icons">home</i>Accueil</a></li>
                    <li><a href="../views/register.php"><i class="material-icons">subscriptions</i>S'inscrire</a></li>
                </ul>
                <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            </div>
        </nav>
        <div class="container">
            <div class="section">
                <form action="../src/login.php" method="post" class="col s12">
                    <div class="input-field col s6">
                        <input id="email" name="email" type="email" class="validate" minlength="2" maxlength="50" pattern="/^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/" required>
                        <label for="email">adresse email</label>
                        <span class="helper-text" data-error="Merci de saisir une adresse e-mail correct"></span>
                    </div>
                    <div class="input-field col s6">
                        <input id="password" name="password" type="password" class="validate" minlength="5" maxlength="50" required>
                        <label for="password">Mot de passe</label>
                        <span class="helper-text" data-error="Minimum 5 caractères et maximum 50 caractères"></span>
                    </div>
                    <div class="row center">
                        <input type="submit" class="btn-large waves-light teal lighten-1" content="Se connecter">
                    </div>
                </form>
                <div class="row center">
                    Pas encore inscrit ? <br />
                    Rejoignez nous vite! <br />
                    <a href="register.php" class="btn-large waves-effect waves-light teal lighten-1">s'incrire</a>
                </div>
            </div>
        </div>
    </body>
</html>